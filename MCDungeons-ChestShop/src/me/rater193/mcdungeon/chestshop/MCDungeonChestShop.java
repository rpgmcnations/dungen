package me.rater193.mcdungeon.chestshop;

import org.bukkit.Material;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import me.rater193.mcdungeon.items.ItemHelper;
import me.rater193.mcdungeon.items.abilities.RPGAbility;
import me.rater193.mcdungeon.items.items.RPGItem;
import me.rater193.mcdungeon.items.items.RPGItemSkillScroll;
import net.md_5.bungee.api.ChatColor;

public class MCDungeonChestShop extends JavaPlugin {

	// Feel free to change this to your own plugin's name and color of your choice.
	public static final String CHAT_PREFIX = ChatColor.AQUA + "MCDungeonChestShop";

	private static MCDungeonChestShop plugin; // This is a static plugin instance that is private. Use getPlugin() as seen
									// further below.

	PluginDescriptionFile pdfFile; // plugin.yml

	// Called when the plugin is disabled, such as when you reload the server.
	public void onDisable() {
		
	}

	public static MCDungeonChestShop getPlugin() { // getter for the static plugin instance
		return plugin;
	}
	
	// Called when the plugin is enabled. It is used to set up variables and to register things such as commands.
	@Override
	public void onEnable() {
		//String itemName, int itemWorth, Material materialType, int modeldata
		//Registering basic items
		
		RPGAbility ability = new RPGAbility();
		ability.displayName = "Test ability";
		ability.description = "This is just a test debug ability! Get it while you can, it will be un achievable in the future!";
		RPGAbility.registerAbility("TestAbility", ability);
		
		ItemHelper.registerItem("basicitem_1", new RPGItem("Test item 1", 100, Material.PAPER, 0));
		ItemHelper.registerItem("basicitem_2", new RPGItem("Test item 2", 500, Material.PAPER, 0));
		ItemHelper.registerItem("basicitem_3", new RPGItem("Test item 3", 1092, Material.APPLE, 0));
		ItemHelper.registerItem("basicitem_4", new RPGItem("Test item 4", 1, Material.BREAD, 0));
		ItemHelper.registerItem("TestAbility1", new RPGItemSkillScroll("Test ability", 1, Material.PAPER, 1, "TestAbility"));
		
		plugin = getPlugin(MCDungeonChestShop.class);
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(MerchantTrait.class).withName("merchant"));
		this.getCommand("chestshop").setExecutor(new CmdChestshop());
		this.getCommand("shop").setExecutor(new CmdChestshop());

	}
}
