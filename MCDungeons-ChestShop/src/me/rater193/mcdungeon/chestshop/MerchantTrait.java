package me.rater193.mcdungeon.chestshop;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import me.rater193.mcdungeon.economy.CmdMoney;
import me.rater193.mcdungeon.items.ItemHelper;
import me.rater193.mcdungeon.items.items.RPGItem;
//import me.rater193.mcdungeon.items.abilities.RPGAbility;
import me.rater193.mcdungeon.menus.ChestMenu;
import me.rater193.mcdungeon.menus.ChestMenuItemAction;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;
import net.md_5.bungee.api.ChatColor;

public class MerchantTrait extends Trait {
	
	//Main vars
	public MCDungeonChestShop plugin;
	
	public ArrayList<String> shopItems = new ArrayList<String>();
	
	public ChestMenu menu;
	
	public MerchantTrait() {
		super("merchant");
		shopItems.add("basicitem_1");
		shopItems.add("basicitem_2");
		shopItems.add("basicitem_3");
		shopItems.add("basicitem_4");
		shopItems.add("TestAbility1");
		
		plugin = MCDungeonChestShop.getPlugin();
		
		update();
		/*
		menu.createMenuItem(1, stack, new Runnable() {

			@Override
			public void run() {
				System.out.println("Click?");
			}
			
		});
		 */
		
		/*menu.setItem(0, Material.EMERALD, 0, "[Sword of a thousand cuts]", new Runnable() {

			@Override
			public void run() {
				System.out.println("Buy?");
			}
			
		});*/
	}
	
	public void update() {
		menu = new ChestMenu(18, "Test Merchant Plugin");
		int place = 0;
		for(String itemid : shopItems) {
			ItemStack stack = ItemHelper.getItem(itemid);
			
			menu.inv.addItem(stack);
			menu.createMenuItem(place, stack, new ChestMenuItemAction() {
				@Override
				public void onPlayerClick(Player p) {
					long balance = CmdMoney.getPlayerBalance(p);
					System.out.println("Money: " + balance);
					RPGItem rpgitem = ItemHelper.items.get(itemid);
					if(balance >= rpgitem.shopValue) {
						balance -= rpgitem.shopValue;
						CmdMoney.setPlayerMoney(p.getName(), ""+balance);
						p.getInventory().addItem(ItemHelper.getItem(itemid));
						p.sendMessage(ChatColor.BOLD + " " + ChatColor.GOLD + "You just bought " +rpgitem.itemName + " for " + rpgitem.shopValue + " gold!");
						p.sendMessage(ChatColor.BOLD + " " + ChatColor.GOLD + "You have " + balance + " left!");
					}
				}
			});
			
			place++;
		}
	}
	
	public void load(DataKey key) {
		//SomeSetting = key.getBoolean("SomeSetting", false);
	}

	// Save settings for this NPC (optional). These values will be persisted to the Citizens saves file
	public void save(DataKey key) {
		//key.setBoolean("SomeSetting",SomeSetting);
		//key.setInt("Size", menu.inv.getSize());
	}
	
	@EventHandler
	public void click(net.citizensnpcs.api.event.NPCRightClickEvent event){
		update();
		event.getClicker().openInventory(menu.inv);
	}
	
	@Override
	public void onAttach() {
		plugin.getServer().getLogger().info(npc.getName() + " has been assigned Merchant!");
	}
	
}
