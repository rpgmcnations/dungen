package me.rater193.mcdungeon.chestshop;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.citizensnpcs.api.CitizensAPI;

public class CmdChestshop implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player)sender;
			
			if(CitizensAPI.getDefaultNPCSelector().getSelected(sender)!=null) {
				player.sendMessage("Selected NPC: " + CitizensAPI.getDefaultNPCSelector().getSelected(sender).getName());
			}else {
				player.sendMessage("You must have a npc selected to use this command!");
				player.sendMessage("/npc list");
				player.sendMessage("/npc sel <ID>");
			}
		}else {
			System.out.println("This command can only be sent by a player!");
		}
		return true;
	}

}
