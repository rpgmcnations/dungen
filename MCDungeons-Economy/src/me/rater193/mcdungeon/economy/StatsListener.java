package me.rater193.mcdungeon.economy;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;


public class StatsListener implements Listener {

	@EventHandler
	public void onPlayerJoined(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		PersistentDataContainer data = p.getPersistentDataContainer();
		
		NamespacedKey keyMoney = new NamespacedKey(MCDungeonEconomy.getPlugin(), "money");
		
		if(!data.has(keyMoney, PersistentDataType.LONG)) {
			data.set(keyMoney, PersistentDataType.LONG, 100L);
		}
	}
}
