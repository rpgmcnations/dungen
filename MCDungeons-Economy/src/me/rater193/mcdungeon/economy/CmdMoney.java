package me.rater193.mcdungeon.economy;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class CmdMoney implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		// TODO Auto-generated method stub
		if(sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length==0) {
				showPlayerBalance(player);
			}else {
				switch(args[0]) {
				case "set":
					if(player.hasPermission("economy.admin")) {
						if(args.length>=3) {
							setPlayerMoney(args[1], args[2]);
						}else {
							player.sendMessage("Not enough arguments! /money set <player> <amount>");
						}
					}else {
						player.sendMessage("You do not have permission to use this command!");
					}
					break;
					
				case "give":
					if(player.hasPermission("economy.admin")) {
						if(args.length>=3) {
							givePlayerMoney(args[1],args[2]);
						}else {
							player.sendMessage("Not enough arguments! /money give <player> <amount>");
						}
					}else {
						player.sendMessage("You do not have permission to use this command!");
					}
					break;
					
				case "pay":
					if(args.length>=3) {
						payPlayer(player, args[1], args[2]);
					}else {
						player.sendMessage("Not enough arguments! /money pay <player> <amount>");
					}
					break;
					
				default:
					Player targetPlayer = getPlayer(args[0]);
					if(targetPlayer==null) {
						player.sendMessage("Player is not online, or could not be found. " + args[0]);
					}else {
						PersistentDataContainer data = player.getPersistentDataContainer();
						
						player.sendMessage(args[0] + "'s money: " + data.get(getKey(), PersistentDataType.LONG));
					}
					break;
				}
			}
		}
		return true;
	}
	
	public static Player getPlayer(String playerName) {
		for(Player p : Bukkit.getServer().getOnlinePlayers()) {
			if(p.getDisplayName().equalsIgnoreCase(playerName) ||
					p.getName().equalsIgnoreCase(playerName)
					) {
				return p;
			}
		}
		return null;
	}
	
	public static NamespacedKey getKey() {
		return new NamespacedKey(MCDungeonEconomy.getPlugin(), "money");
	}

	public static void payPlayer(Player player, String playername, String amountstr) {
		try {
		Player targetPlayer = getPlayer(playername);
		if(targetPlayer!=null && targetPlayer!=player) {
			PersistentDataContainer p1data = player.getPersistentDataContainer();
			PersistentDataContainer p2data = targetPlayer.getPersistentDataContainer();
			NamespacedKey key = getKey();
			long targetamount = Long.parseLong(amountstr);
			
			if(p1data.has(key, PersistentDataType.LONG) && p2data.has(key, PersistentDataType.LONG) && targetamount>0) {
				long p1bal = p1data.get(key, PersistentDataType.LONG);
				long p2bal = p2data.get(key, PersistentDataType.LONG);
				Player p1 = player;
				Player p2 = targetPlayer;
				
				if(p1bal>=targetamount) {
					p1bal -= targetamount;
					p2bal += targetamount;
					p1data.set(key, PersistentDataType.LONG, p1bal);
					p2data.set(key, PersistentDataType.LONG, p2bal);
					p1.sendMessage("You sent " + p2.getDisplayName() + " " + targetamount + " money!");
					p2.sendMessage("You received " + targetamount + " money from " + p1.getDisplayName() + "!");
				}else {
					p1.sendMessage("You do not have enough money! ( " + p1bal + " money )");
				}
			}
			
		}else {
			player.sendMessage("Player is not online, or could not be found. " + playername);
		}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("=========================================");
			System.out.println("Quick error reference: " + e.getLocalizedMessage());
		}
	}

	public static void givePlayerMoney(String playername, String amountstr) {
		try {
			Player targetPlayer = getPlayer(playername);
			if(targetPlayer!=null) {
				PersistentDataContainer pdata = targetPlayer.getPersistentDataContainer();
				NamespacedKey key = getKey();
				long targetamount = Long.parseLong(amountstr);
				pdata.set(key, PersistentDataType.LONG, pdata.get(key, PersistentDataType.LONG) + targetamount);
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("=========================================");
			System.out.println("Quick error reference: " + e.getLocalizedMessage());
		}
	}

	public static void setPlayerMoney(String playername, String amountstr) {
		try {
			Player targetPlayer = getPlayer(playername);
			if(targetPlayer!=null) {
				PersistentDataContainer pdata = targetPlayer.getPersistentDataContainer();
				NamespacedKey key = getKey();
				long targetamount = Long.parseLong(amountstr);
				pdata.set(key, PersistentDataType.LONG, targetamount);
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("=========================================");
			System.out.println("Quick error reference: " + e.getLocalizedMessage());
		}
	}

	public static void showPlayerBalance(Player player) {
		
		player.sendMessage("Money: " + getPlayerBalance(player));
	}
	
	public static long getPlayerBalance(Player player) {

		PersistentDataContainer data = player.getPersistentDataContainer();

		NamespacedKey keyMoney = getKey();
		
		long money = data.get(keyMoney, PersistentDataType.LONG);
		
		return money;
	}

}
