package me.rater193.mcdungeon.economy;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import net.md_5.bungee.api.ChatColor;

public class MCDungeonEconomy  extends JavaPlugin {

	// Feel free to change this to your own plugin's name and color of your choice.
	public static final String CHAT_PREFIX = ChatColor.AQUA + "MCDungeonEconomy";

	private static MCDungeonEconomy plugin; // This is a static plugin instance that is private. Use getPlugin() as seen
									// further below.

	PluginDescriptionFile pdfFile; // plugin.yml

	// Called when the plugin is disabled, such as when you reload the server.
	public void onDisable() {
		
	}

	public static MCDungeonEconomy getPlugin() { // getter for the static plugin instance
		return plugin;
	}
	
	// Called when the plugin is enabled. It is used to set up variables and to register things such as commands.
	@Override
	public void onEnable() {
		plugin = getPlugin(MCDungeonEconomy.class);
		PluginManager pm = getServer().getPluginManager();
		getCommand("econ").setExecutor(new CmdMoney());
		getCommand("money").setExecutor(new CmdMoney());
		getCommand("m").setExecutor(new CmdMoney());
		getCommand("bal").setExecutor(new CmdMoney());
		getCommand("cash").setExecutor(new CmdMoney());
		pm.registerEvents(new StatsListener(), this);
	}
}
