package me.rater193.plugin;

import org.bukkit.scheduler.BukkitRunnable;

public class QueuedTaskManager {
	
	public static int state = 0;
	static QueuedAction nextAction;
	
	public static void Init() {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
				int maxIterations = 4;
				while (!GameHelper.queue.isEmpty() && maxIterations > 0) {
					QueuedAction r = GameHelper.queue.get(0);
					r.run();
					GameHelper.queue.remove(0);
					maxIterations--;
				}
            }
        }.runTaskTimer(DunGEN.getPlugin(), 1, 1);
		/*
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DunGEN.getPlugin(), new Runnable(){
			public void run(){
				
				int maxIterations = 4;
				while (!Debug.queue.isEmpty() && maxIterations > 0) {
					QueuedAction r = Debug.queue.get(0);
					r.run();
					Debug.queue.remove(0);
					maxIterations--;
				}
				
			}
		}, 1l, 1l);*/
	}
}
