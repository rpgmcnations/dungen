package me.rater193.plugin.command;

import static me.rater193.plugin.DunGEN.CHAT_PREFIX;

import java.util.Hashtable;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rater193.plugin.command.subcommands.*;
import net.md_5.bungee.api.ChatColor;

// All command classes need to implement the CommandExecutor interface to be a proper command!
public class Dungeon implements CommandExecutor {
	
	public Hashtable<String, SubCommand> subcommands = new Hashtable<String, SubCommand>();
	
	public Dungeon() {
		subcommands.put("create", new SubCmdCreate());
		subcommands.put("delete", new SubCmdDelete());
		//subcommands.put("edit", new SubCmdEdit());
		subcommands.put("list", new SubCmdList());
		
		subcommands.put("generate", new SubCmdGenerate());
		subcommands.put("gen", new SubCmdGenerate());
		subcommands.put("temp", new SubCmdTemplate());
		subcommands.put("template", new SubCmdTemplate());
		subcommands.put("test", new SubCmdTest());
		subcommands.put("t", new SubCmdTest());
	}

	/* Called when the command is ran
	args variable is the commands arguments in an array of strings.
	sender variable is the sender who ran the command
	*/
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {

		/* Checks if the sender is sending from the console
		   CHAT_PREFIX is the variable from the Template plugin class and can be removed
		   if unwanted. 
		*/
		Player player = null;
		if ((sender instanceof Player)) {
			player = (Player) sender;
		}
		/*
		 * Since we made sure the sender is a player, we can create a new player
		 * variable using our sender
		 */
		if(args.length >= 1) {
			if(subcommands.containsKey(args[0].toLowerCase())) {
				subcommands.get(args[0].toLowerCase()).execute(player, sender, cmd, cmdLabel, args);
				return true;
			}else {
				player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " Invalid Sub Command");
				player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d list <dungeons|brushes|d|b>");
				player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d gen <brush> <dungeontileX> <dungeontileY> <x> <y> <z>");
				player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d template");
			}
		}else {
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " Not enough arguments");
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d list <dungeons|brushes|d|b>");
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d gen <brush> <dungeontileX> <dungeontileY> <x> <y> <z>");
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " /d template");
		}
		/*
		for(int index = 0; index < args.length; index++) {
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + "" + args[index]);
		}

		// checks if there are no arguments at all (/command)
		if (args.length == 0) {
			player.sendMessage(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + "Invalid usage - there are no arguments.");
			return false;
		}
		*/

		/*
		 * checks if First argument (/command FIRSTARGUMENT) is equal to the string In
		 * this case, the command would be: /example sayhi
		 */
		/*
		if (args[0].equalsIgnoreCase("sayhi")) {
		 	*/
			/*
			 * makes the player chat the message, Hello Everyone! colored light green.
			 */
			/*player.chat(ChatColor.GREEN + "Hello Everyone!");

			return true;
		} else {
			player.sendMessage(CHAT_PREFIX + ChatColor.WHITE + " > " + ChatColor.RED + "Invalid usage or arguments.");
			return false;
		}*/
		return false;
	}

}
