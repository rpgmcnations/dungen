package me.rater193.plugin.command.subcommands;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rater193.plugin.GameHelper;
import me.rater193.plugin.DunGEN;

public class SubCmdDelete extends SubCommand {

	public void execute(Player player, CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		super.execute(player, sender, cmd, cmdLabel, args);
		if(!player.hasPermission("DunGEN.command.delete")) return;
		if(args.length >= 2) {
			try {
				deleteDirectoryRecursionJava(new File(DunGEN.getBrushesDirectory() + "" + args[1]));
				GameHelper.print("Successfully deleted brush: " + args[1]);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				GameHelper.print("Failed to delete brush: " + args[1]);
				GameHelper.print(e.getLocalizedMessage());
			}
		}else {
			GameHelper.print("Not enough arguments!");
			GameHelper.print("/d delete <brushName>");
		}
	}
	void deleteDirectoryRecursionJava(File file) throws IOException {
		if (file.isDirectory()) {
			File[] entries = file.listFiles();
			if (entries != null) {
				for (File entry : entries) {
					deleteDirectoryRecursionJava(entry);
				}
			}
		}
		if (!file.delete()) {
			throw new IOException("Failed to delete " + file);
		}
	}
}
