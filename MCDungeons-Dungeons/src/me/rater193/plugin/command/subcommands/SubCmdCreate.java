package me.rater193.plugin.command.subcommands;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.json.simple.JSONObject;

import com.google.common.io.Files;

import me.rater193.plugin.DunGEN;

public class SubCmdCreate extends SubCommand {

	@SuppressWarnings("unchecked")
	public void execute(Player player, CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		super.execute(player, sender, cmd, cmdLabel, args);
		if(!player.hasPermission("DunGEN.command.create")) return;
		
		if(args.length<4) {
			player.sendMessage("Not enough arguments");
			player.sendMessage("/d create <WorldEditBrushName> <DungeonBrushName> <TilePartSize>");
		}else {
			String brushPath = DunGEN.getBrushesDirectory() + args[2] + "/";
			String weSchematicsPath = DunGEN.getPlugin().getDataFolder() + "/../WorldEdit/schematics/";
			player.sendMessage("brushPath: " + brushPath);
			player.sendMessage("weSchematicsPath: " + weSchematicsPath);
			
			player.sendMessage("args[0]" + args[0] + ", "
					+ "args[1]" + args[1] + ", "
					+ "args[2]" + args[2] + ", "
					+ "args[3]" + args[3] + ", ");
			
			
			File weSchematic = new File(weSchematicsPath + args[1]);
			if(weSchematic.exists() && weSchematic.isFile()) {
				player.sendMessage("schematic exists!");
				File fBrushDir = new File(brushPath);
				player.sendMessage(brushPath + "/" + args[1] + "/");
				if(!fBrushDir.exists()) {
					//JsonObject obj = new JsonObject();
					//obj.addProperty(property, value);
					fBrushDir.mkdirs();
					try {
						Files.copy(weSchematic, new File(brushPath + "skin.schem"));
						
						
						FileWriter fw = new FileWriter(brushPath + "dungeon.json");
						
						JSONObject obj = new JSONObject();
						obj.put("TilePartSize", Integer.parseInt(args[3]));
						
						fw.write(obj.toJSONString());
						fw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {
					player.sendMessage("Brush already exists!");
					player.sendMessage("/d list brushes");
					player.sendMessage("/d delete " + args[2]);
				}
			}else {
				player.sendMessage("Schematic does not exsist!");
			}
		}
		
	}
}
