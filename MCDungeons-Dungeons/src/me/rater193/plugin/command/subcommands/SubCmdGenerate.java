package me.rater193.plugin.command.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


import me.rater193.plugin.GameHelper;
import me.rater193.plugin.dungeongenerator.DungeonGenerator;

public class SubCmdGenerate extends SubCommand {
	
	@SuppressWarnings("unused")
	public void execute(Player player, CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		
		super.execute(player, sender, cmd, cmdLabel, args);
		if(!player.hasPermission("DunGEN.command.generate")) return;
		
		GameHelper.print("Generating " + args.length);
		DungeonGenerator dgen = null;
		
		if(args.length < 7) {
			if(player!=null) {
				dgen = new DungeonGenerator(
					(int)player.getLocation().getX(),
					(int)player.getLocation().getY(),
					(int)player.getLocation().getZ()
					);
			}else {
				GameHelper.print("Must be a player to issue command without coordinates!");
				GameHelper.print("/d gen SkinName SizeX SizeY X Y Z");
			}
		}else {
				dgen = new DungeonGenerator(
						Integer.parseInt(args[4]),
						Integer.parseInt(args[5]),
						Integer.parseInt(args[6])
					);
		}
		
		if(dgen!=null) {
			if(args.length>=4) {
				GameHelper.print(args[1], args[2], args[3]);
				dgen.Generate(args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
			}else {
				dgen.Generate();
			}
		}
	}
}
