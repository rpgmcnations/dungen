package me.rater193.plugin.command.subcommands;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rater193.plugin.GameHelper;

public class SubCmdTemplate extends SubCommand {

	public void execute(Player player, CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		
		super.execute(player, sender, cmd, cmdLabel, args);

		if(!player.hasPermission("DunGEN.command.template")) return;
		
		try {
	
			int tileSize = args.length < 2 ? 3 : Integer.parseInt(args[1]);
			int sizeX = 4;
			int sizeY = 10;
	
			for(int _x = 0; _x < sizeX*tileSize; _x++) {
				for(int _y = 0; _y < sizeY*tileSize; _y++) {
					
					Location loc =  player.getLocation().clone();
					loc.add(_x, 0, _y);
					
					Material mat = ((_x/tileSize) + ((_y/tileSize) % 2)) % 2 == 0 ? Material.BLACK_CONCRETE : Material.WHITE_CONCRETE;
					player.getWorld().getBlockAt(loc).setType(mat);
				}
			}
		}catch(Exception e) {
			GameHelper.print("Error: " + e.getLocalizedMessage().toString());
			e.printStackTrace();
		}
	}
}
