package me.rater193.plugin.command.subcommands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rater193.plugin.DunGEN;
import net.md_5.bungee.api.ChatColor;

public class SubCmdList extends SubCommand {
	///////////////////////////////////////////////////////////
	//    THIS DISPLAYS ALL THE AVAILABLE BRUSHES TO USE     //
	///////////////////////////////////////////////////////////
	public void execute(Player player, CommandSender sender, Command cmd, String cmdLabel, String[] args) {

		if(!player.hasPermission("DunGEN.command.list")) return;
		if(args.length >= 2) {
			String filePath;
			switch(args[1]) {
			case "brushes":
				listBrushes(player);
				break;
			case "b":
				listBrushes(player);
				break;
				
			case "dungeons":
				listDungeons(player);
				break;
				
			case "d":
				listDungeons(player);
				break;
				
			default:
				player.sendMessage("Please specify what to list!");
				player.sendMessage(getUsageExample());
				break;
			}
		}else {
			player.sendMessage("Not enough arguments!");
			player.sendMessage(getUsageExample());
		}
		
		
		
		super.execute(player, sender, cmd, cmdLabel, args);
		
	}
	
	private void listDungeons(Player player) {
		String filePath = DunGEN.getDungeonsDirectory();
	
		try {
			player.sendMessage(ChatColor.GREEN + "===============================================");
			//Retrieves the list of brushes
			Files.list(new File(filePath).toPath()).forEach(path -> {
				File f = new File(path + "");
				if(f.isFile()) {
					player.sendMessage(ChatColor.DARK_GREEN + "Brush name: " + f.getName());
				}
			});
			player.sendMessage(ChatColor.GREEN + "===============================================");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void listBrushes(Player player) {
		String filePath = DunGEN.getBrushesDirectory();
	
		try {
			player.sendMessage(ChatColor.GREEN + "===============================================");
			//Retrieves the list of brushes
			Files.list(new File(filePath).toPath()).forEach(path -> {
				File f = new File(path + "");
				if(f.isDirectory()) {
					player.sendMessage(ChatColor.DARK_GREEN + "Brush name: " + f.getName());
				}
			});
			player.sendMessage(ChatColor.GREEN + "===============================================");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	String getUsageExample() {
		return ChatColor.BLUE + "/d list " + ChatColor.RED + "<" + ChatColor.YELLOW + "dungeons" + ChatColor.RED + "|" + ChatColor.YELLOW + "brushes" + ChatColor.RED + ">";
	}
}
