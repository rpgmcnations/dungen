package me.rater193.plugin;

import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.rater193.plugin.command.Dungeon;
import me.rater193.plugin.listener.ExampleListener;
import net.md_5.bungee.api.ChatColor;

/**
 * 
 * @author Creepinson101
 * 
**/
public class DunGEN extends JavaPlugin {

	// Feel free to change this to your own plugin's name and color of your choice.
	public static final String CHAT_PREFIX = ChatColor.AQUA + "DunGEN";

	private static DunGEN plugin; // This is a static plugin instance that is private. Use getPlugin() as seen
									// further below.

	PluginDescriptionFile pdfFile; // plugin.yml

	// Called when the plugin is disabled, such as when you reload the server.
	public void onDisable() {
		
	}

	public static DunGEN getPlugin() { // getter for the static plugin instance
		return plugin;
	}
	
	// Called when the plugin is enabled. It is used to set up variables and to register things such as commands.
	@Override
	public void onEnable() {
		plugin = getPlugin(DunGEN.class);
		PluginManager pm = getServer().getPluginManager();
		System.out.println("1");
		/*
		 * Register a command to the list of usable commands. If you don't register the
		 * command, it won't work! Also if you change the command name, make sure to
		 * also change in the plugin.yml file.
		 */
		this.getCommand("dungeon").setExecutor(new Dungeon());
		this.getCommand("d").setExecutor(new Dungeon());
		System.out.println("2");

		/*
		 * Make sure you register your listeners if you have any! If you have a class
		 * that implements Listener, you need to make sure to register it. Otherwise it
		 * will DO NOTHING!
		 */
		System.out.println("3");
		pm.registerEvents(new ExampleListener(), this);
		
		QueuedTaskManager.Init();

		//getServer().createWorld(new ArcticWorldCreator("Arctic"));
		//new ArcticWorldCreator("Arctic").createWorld();
		WorldCreator wc = new WorldCreator("Arctic");
		wc.environment(Environment.NORMAL);
		wc.generator(new ArcticGenerator());
		wc.createWorld();
		System.out.println("4");
		
	}
	
	public static String getBrushesDirectoryName() {
		return "brushes/";
	}
	
	public static String getDungeonsDirectoryName() {
		return "dungeons/";
	}

	public static String getBrushesDirectory() {
		// TODO Auto-generated method stub
		return getPlugin().getDataFolder() + "/" + getBrushesDirectoryName();
	}

	public static String getDungeonsDirectory() {
		// TODO Auto-generated method stub
		return getPlugin().getDataFolder() + "/" + getDungeonsDirectoryName();
	}

}
