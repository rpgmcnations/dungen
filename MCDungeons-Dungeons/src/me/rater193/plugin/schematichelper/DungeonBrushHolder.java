package me.rater193.plugin.schematichelper;

import java.io.File;
import java.util.Hashtable;

import org.json.simple.JSONObject;

import me.rater193.plugin.GameHelper;
import me.rater193.plugin.DunGEN;
import me.rater193.plugin.QueuedAction;

public class DungeonBrushHolder {
	
	
	public final String brushPath;
	public SchematicHolder schematic;
	public int tileSize = -1;
	public int schematicHeight = -1;
	public Hashtable<String, SchematicHolder> prefabs;
	String brushName = "";
	
	@SuppressWarnings("unchecked")
	public DungeonBrushHolder(String brushName) {
		this.brushName = brushName;
		//Loading the schematic
		brushPath = DunGEN.getBrushesDirectoryName() + brushName + "/";
		
		//Loadking skin
		schematic = SchematicHelper.loadSchematic(brushPath + "skin.schem");
		schematicHeight = schematic.clipboard.getMaximumPoint().getBlockY() - schematic.clipboard.getMinimumPoint().getY();
		
		//////////////////////////////////////////////////////
		//============== Parsing dungeon json ==============//
		//////////////////////////////////////////////////////
		File file = new File(DunGEN.getPlugin().getDataFolder() + "/" + brushPath + "/dungeon.json");
		
		JSONObject json = GameHelper.loadJsonFile(file);
		tileSize = (int)((long) json.getOrDefault("TilePartSize", 3));
		
		System.out.println("Loading props, file-listing!");
		//Loading props
		File[] files = GameHelper.getFilesInDirectory(DunGEN.getPlugin().getDataFolder() + "/" + brushPath + "/props/", "json");
		if(files!=null) {
			for(File f : files) {
				System.out.println(f.getAbsolutePath());
			}
		}
		
		System.out.println("Loading rooms, file-listing!");
		//Loading rooms
		files = GameHelper.getFilesInDirectory(DunGEN.getPlugin().getDataFolder() + "/" + brushPath + "/rooms/", "json");
		if(files!=null) {
			for(File f : files) {
				System.out.println(f.getAbsolutePath());
			}
		}
	}
	
	public void placeDungeonTilePart(int worldX, int worldY, int worldZ, int tileX, int tileY) {
			GameHelper.queue.add(new QueuedAction(){
				public void run(){
					schematic = SchematicHelper.loadSchematic("/" + DunGEN.getBrushesDirectoryName() + brushName + "/skin.schem");
					schematic.PlaceSchematicPart(worldX, worldY, worldZ, tileSize * tileX, 0, tileSize * tileY, tileSize-1, 10, tileSize-1);
				}
			}
		);
		//schematic.PlaceSchematicPart(worldX, worldY, worldZ, tileSize * tileX, 0, tileSize * tileY, tileSize-1, 10, tileSize-1);
	}
}
