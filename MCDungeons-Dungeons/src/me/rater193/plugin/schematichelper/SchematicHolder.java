package me.rater193.plugin.schematichelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import org.bukkit.Bukkit;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.util.io.Closer;

import me.rater193.plugin.DunGEN;

public class SchematicHolder {
	
	EditSession extent;
	WorldEditPlugin worldEdit;
	Clipboard clipboard;
	ClipboardHolder holder;

	//Loading the file, and assigning essential files from the file
	public SchematicHolder(String filePath) {
		//Loading the schematic
		try {
			extent = WorldEdit.getInstance().getEditSessionFactory().getEditSession(new BukkitWorld(Bukkit.getServer().getWorld("World")), -1);
			worldEdit = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
			File schematic = new File(DunGEN.getPlugin().getDataFolder() + "/" + filePath);
			//ClipboardFormats.findByAlias(formatName);
	        ClipboardFormat format = ClipboardFormats.findByFile(schematic);
			
			Closer closer = Closer.create();
	        FileInputStream fis = closer.register(new FileInputStream(schematic));
	        BufferedInputStream bis = closer.register(new BufferedInputStream(fis));
	        ClipboardReader reader = closer.register(format.getReader(bis));
	        
	        clipboard = reader.read();
	        holder = new ClipboardHolder(clipboard);
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//Placing the schematic in the world
	public void placeSchematic(int x, int y, int z) {
        try {
            BlockVector3 to = BlockVector3.at
            		(
            				x + (clipboard.getOrigin().getX() - clipboard.getMinimumPoint().getX()),
            				y + (clipboard.getOrigin().getY() - clipboard.getMinimumPoint().getY()),
            				z + (clipboard.getOrigin().getZ() - clipboard.getMinimumPoint().getZ())
            				);
            
    		ForwardExtentCopy copy = new ForwardExtentCopy(clipboard, clipboard.getRegion(), clipboard.getOrigin(), extent, to);
			Operations.completeLegacy(copy);
	        extent.flushSession();
		} catch (MaxChangedBlocksException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void PlaceSchematicPart(int x, int y, int z, int xoff, int yoff, int zoff, int xsize, int ysize, int zsize) {
        try {

            BlockVector3 to = BlockVector3.at
            		(
            				x + (clipboard.getOrigin().getX() - clipboard.getMinimumPoint().getX()) - xoff,
            				y + (clipboard.getOrigin().getY() - clipboard.getMinimumPoint().getY()) - yoff,
            				z + (clipboard.getOrigin().getZ() - clipboard.getMinimumPoint().getZ() - zoff)
            				);
            
	        Region r = new CuboidRegion(
	        		BlockVector3.at(
	        				(int)(clipboard.getMinimumPoint().getX()+xoff),
	        				(int)(clipboard.getMinimumPoint().getY()+yoff),
	        				(int)(clipboard.getMinimumPoint().getZ()+zoff)
			        		),
	        		BlockVector3.at(
	        				(int)(clipboard.getMinimumPoint().getX()+xoff) + xsize,
	        				(int)(clipboard.getMinimumPoint().getY()+yoff) + ysize,
	        				(int)(clipboard.getMinimumPoint().getZ()+zoff) + zsize
	        				)
	        		);
            
    		ForwardExtentCopy copy = new ForwardExtentCopy(clipboard, r, clipboard.getOrigin(), extent, to);
			Operations.completeLegacy(copy);
	        extent.flushSession();
		} catch (MaxChangedBlocksException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
