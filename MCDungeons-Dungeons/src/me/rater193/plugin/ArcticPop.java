package me.rater193.plugin;

import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.TreeType;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

public class ArcticPop extends BlockPopulator
{
	@Override
	public void populate(World world, Random rand, Chunk chunk)
	{
		int x = chunk.getX()*16+5+rand.nextInt(7), z = chunk.getZ()*16+5+rand.nextInt(7);
		int y = world.getHighestBlockYAt(x, z);
		TreeType type = rand.nextInt(10) < 3 ? TreeType.TALL_REDWOOD : TreeType.REDWOOD;
		world.generateTree(world.getBlockAt(x, y, z).getLocation(), type);
	}
}