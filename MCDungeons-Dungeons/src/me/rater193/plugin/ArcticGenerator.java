package me.rater193.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
 
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
 
public class ArcticGenerator extends ChunkGenerator
{
	@Override
	public boolean canSpawn(World world, int x, int z) {
		// TODO Auto-generated method stub
		return super.canSpawn(world, x, z);
	}
	@Override
	public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
		// TODO Auto-generated method stub
		return super.generateChunkData(world, random, x, z, biome);
	}
	@Override
	public boolean isParallelCapable() {
		// TODO Auto-generated method stub
		return true;
	}
	public ArcticGenerator()
	{
	}
	@Override
	public List<BlockPopulator> getDefaultPopulators(World world)
	{
		ArrayList<BlockPopulator> pop =  new ArrayList<BlockPopulator>();
		pop.add(new ArcticPop());
		return pop;
	}
	@Override
	public Location getFixedSpawnLocation(World world, Random rand)
	{
		return new Location(world, 0, world.getHighestBlockYAt(0, 0)+2, 0);
	}
	public int coordsToIndex(int x, int y, int z)
	{
		return (x*16+z)*128+y;
	}
	
	public byte[] generate(World world, Random rand, int chunkx, int chunkz)
	{
		byte[] blocks = new byte[32768];
		/*SimplexOctaveGenerator octave = new SimplexOctaveGenerator(new Random(world.getSeed()), 8);
		octave.setScale(1.0 / 128.0);
		int x, y, z;
		for(x=0; x<16; x++)
		{
			for(z=0; z<16; z++)
			{
				blocks[coordsToIndex(x,0,z)] = (byte) Material.BEDROCK.getId();
				double noise = octave.noise(x+chunkx*16, z+chunkz*16, 0.5, 0.5) * 3;
				for(y=1; y<64+noise; y++)
				{
					blocks[coordsToIndex(x, y, z)] = (byte) Material.SNOW.getId();
				}
			}
		}*/
		return blocks;
	}
}