package me.rater193.plugin.dungeongenerator;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;

import me.rater193.plugin.GameHelper;
import me.rater193.plugin.QueuedAction;
import me.rater193.plugin.schematichelper.DungeonBrushHolder;


public class DungeonGenerator {
	
	public class DungeonTile {
		int tileSize = 5;
		int
			posX = 0,
			posY = 0,
			posZ = 0;
		
		public boolean
			n = false,
			s = false,
			e = false,
			w = false,
			ne = false,
			nw = false,
			se = false,
			sw = false;
		public int tileX;
		public int tileY;
		
		public DungeonTile(int posX, int posY, int posZ) {
			this.posX = posX;
			this.posY = posY;
			this.posZ = posZ;
		}
		
		public void updateTilePreview() {
			
            if (n || w) {
                if (n && !w) {
                    //Debug.placeSchematic(posX, posY, posZ, "0_4");
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 0, 4);
                } else if (!n && w) {
                    //Debug.placeSchematic(posX, posY, posZ, "2_2");
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 2);
                } else if (n && w && !nw) {
                    //Debug.placeSchematic(posX, posY, posZ, "2_0");
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 0);
                } else if (n && w && nw) {
                    //Debug.placeSchematic(posX, posY, posZ, "2_4");
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 4);
                }
            } else {
                //Debug.placeSchematic(posX, posY, posZ, "0_0");
                dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 0, 0);
            }
            posZ += tileSize;
            if(s || w) {
                if (s && !w) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 0, 3);
                    //Debug.placeSchematic(posX, posY, posZ, "0_3");
                }else if (!s && w) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 5);
                    //Debug.placeSchematic(posX, posY, posZ, "2_5");
                }else if (s && w && !sw) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 1);
                    //Debug.placeSchematic(posX, posY, posZ, "2_1");
                }else if (s && w && sw) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 2, 3);
                    //Debug.placeSchematic(posX, posY, posZ, "2_3");
                }
            } else {
                dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 0, 1);
                //Debug.placeSchematic(posX, posY, posZ, "0_1");
            }
            posZ -= tileSize;
            
            posX += tileSize;
            if (n || e) {
                if (n && !e) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 3, 4);
                    //Debug.placeSchematic(posX, posY, posZ, "3_4");
                } else if (!n && e) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 2);
                    //Debug.placeSchematic(posX, posY, posZ, "1_2");
                } else if (n && e && !ne) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 3, 0);
                    //Debug.placeSchematic(posX, posY, posZ, "3_0");
                } else if (n && e && ne) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 4);
                    //Debug.placeSchematic(posX, posY, posZ, "1_4");
                }
            } else {
                dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 0);
                //Debug.placeSchematic(posX, posY, posZ, "1_0");
            }
            posX -= tileSize;
            
            posX += tileSize;
            posZ += tileSize;
            if (s || e) {
                if (s && !e) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 3, 3);
                    //Debug.placeSchematic(posX, posY, posZ, "3_3");
                } else if (!s && e) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 5);
                    //Debug.placeSchematic(posX, posY, posZ, "1_5");
                } else if (s && e && !se) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 3, 1);
                    //Debug.placeSchematic(posX, posY, posZ, "3_1");
                } else if (s && e && se) {
                    dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 3);
                    //Debug.placeSchematic(posX, posY, posZ, "1_3");
                }
            } else {
                dungeonBrush.placeDungeonTilePart(posX, posY, posZ, 1, 1);
                //Debug.placeSchematic(posX, posY, posZ, "1_1");
            }
            posX -= tileSize;
            posZ -= tileSize;
			
		}
	}
	
	public static class Rectangle {
	    private Point bottomLeft;
	    private Point topRight;
	    
	    //Primary constructor
	    public Rectangle(Point bl, Point tr) {
	    	this.bottomLeft = bl;
	    	this.topRight = tr;
	    }
	    
	    //Alternative constructor
	    public Rectangle(int x, int y, int width, int height) {
	    	this.bottomLeft = new Point(x, y);
	    	this.topRight = new Point(x + width, y + height);
	    }
	    
	    public int getWidth() {
	    	return topRight.getX() - bottomLeft.getX();
	    }
	    
	    public int getHeight() {
	    	return topRight.getY() - bottomLeft.getY();
	    }
	    
	    //Checks of two rectangles are overlapping
	    boolean isOverlapping(Rectangle other) {
			if (this.topRight.getY() < other.bottomLeft.getY() || this.bottomLeft.getY() > other.topRight.getY()) {
				return false;
			}
			if (this.topRight.getX() < other.bottomLeft.getX() || this.bottomLeft.getX() > other.topRight.getX()) {
				return false;
			}
			return true;
		}
	}
	
	public static class Point {
	    private int x = 0;
	    private int y = 0;
	    
	    public Point(int x, int y) {
	    	this.x = x;
	    	this.y = y;
	    }
	    
	    public int getX() { return x; }
	    public int getY() { return y; }
	}
	
	public Hashtable<String, DungeonTile> tiles = new Hashtable<String, DungeonTile>();
	
	
	
	int
		posX = 0,
		posY = 0,
		posZ = 0;
	
	DungeonBrushHolder dungeonBrush;
	
	public DungeonGenerator(int posX, int posY, int posZ) {
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
	}
	
    public void setSpawner(Block block) {
    	block.setType(Material.SPAWNER);
        BlockState blockState = block.getState();
        CreatureSpawner spawner = ((CreatureSpawner)blockState);
        spawner.setSpawnedType(EntityType.ZOMBIE);
        //Debug.print(spawner.getPersistentDataContainer().get("Minecraft", ));
        //spawner.setMetadata(arg0, arg1);
        blockState.update();
    }
    
    public void Generate() { Generate("default", 10, 10); }
	
	public void Generate(String skinName, int sizeX, int sizeY) {
		GameHelper.print("Using new version!");
		dungeonBrush = new DungeonBrushHolder(skinName);
		/*
		for(int _x=0; _x<4; _x++) {
			for(int _y=0; _y<2; _y++) {
				dungeonBrush.placeDungeonTilePart(dungeonBrush.tileSize * _x,  100, dungeonBrush.tileSize * _y, _x, _y);
			}
		}*/
		
		Instant start = Instant.now();
		Maze maze = new Maze(sizeX, sizeY);
		
		/*
		//Clearing the whole room
		for(int ___x = 0; ___x < 4; ___x++) {
			for(int ___y = 0; ___y < 4; ___y++) {
				int ___tx = 2+___x, ___ty = 2+___y;
				maze.maze[___tx][___ty] = new Cell(___tx, ___ty);
				maze.maze[___tx][___ty].visited = true;
				maze.maze[___tx][___ty].isRoomTile = true;
				maze.maze[___tx][___ty].left = false;
				maze.maze[___tx][___ty].right = false;
				maze.maze[___tx][___ty].top = false;
				maze.maze[___tx][___ty].bottom = false;
			}
		}*/
		
		//Generating empty rooms
		Rectangle r1 = new Rectangle(0, 0, 2, 2);
		Rectangle r2 = new Rectangle(0, 2, 2, 2);
		Rectangle r3 = new Rectangle(0, 3, 2, 2);
		System.out.println("r1-r2: " + r1.isOverlapping(r2));
		System.out.println("r1-r3: " + r1.isOverlapping(r3));
		ArrayList<Rectangle> rooms = new ArrayList<Rectangle>();
		for(int i = 0; i < 4; i++) {

			int w = new Random().nextInt(3)+2;
			int h = new Random().nextInt(3)+2;
			
			Rectangle newr = new Rectangle(new Random().nextInt(sizeX-w-1)+1,new Random().nextInt(sizeY-h-1)+1,w,h);
			
			boolean isOverlaping = false;
			
			for(Rectangle r : rooms) {
				if(newr.isOverlapping(r)) {
					isOverlaping = true;
					break;
				}
			}
			
			if(!isOverlaping) {
				rooms.add(newr);
			}
		}
		
		
		
		//Pre-claiming room tiles

		for(Rectangle r : rooms) {

			int x1 = r.bottomLeft.x;
			int y1 = r.bottomLeft.y;
			int x2 = x1 + r.getWidth();
			int y2 = y1 + r.getHeight();
			
			//Here we are placing the larger room
			for(int ___x = x1; ___x < x2; ___x++) {
				for(int ___y = y1; ___y < y2; ___y++) {
					int ___tx = ___x, ___ty = ___y;
					maze.maze[___tx][___ty] = new Cell(___tx, ___ty);
					maze.maze[___tx][___ty].visited = true;
					maze.maze[___tx][___ty].roomrect = r;
				}
			}
		}
		

		//maze.maze[0][0].top = false;
		//maze.maze[0][0].bottom = false;
		
		maze.generateMaze();
		
		/*
		 * 
		 * Updates the rooms look
		 * 
		 */

		
		//Updating maze connectors
		for(Rectangle r : rooms) {

			int x1 = r.bottomLeft.x;
			int y1 = r.bottomLeft.y;
			int x2 = x1 + r.getWidth();
			int y2 = y1 + r.getHeight();
			
			//Here we are placing the larger room
			for(int ___x = x1; ___x < x2; ___x++) {
				for(int ___y = y1; ___y < y2; ___y++) {
					int ___tx = ___x, ___ty = ___y;
					maze.maze[___tx][___ty].isRoomTile = true;
					maze.maze[___tx][___ty].left = false;
					maze.maze[___tx][___ty].right = false;
					maze.maze[___tx][___ty].top = false;
					maze.maze[___tx][___ty].bottom = false;
				}
			}
			
			//Updating north and south walls
			for(int ___x = x1; ___x < x2; ___x++) {
				maze.maze[___x][y1].top = true;
				maze.maze[___x][y2-1].bottom = true;
			}
			
			for(int ___y = y1; ___y < y2; ___y++) {
				maze.maze[x1][___y].left = true;
				maze.maze[x2-1][___y].right = true;
			}
			
			//WALLS
			maze.maze[x1][y1].left = false;
			maze.maze[x1-1][y1].right = false;
			maze.maze[x2-1][y2-1].right = false;
			maze.maze[x2][y2-1].left = false;
		}
		

		int roomWidth = maze.maze.length;
		int roomHeight = maze.maze[1].length;
		/*
		 * 
		 * Randomly connects tiles, to give a better feel for the generation
		 * 
		 */
		for(int i = (int)((float)(roomWidth*roomHeight)*(0.2f)); i > 0; i -= 1) {
			int x = (int)(((double)(Math.random())*(roomWidth-2)))+1;
			int y = (int)(((double)(Math.random())*(roomHeight-2)))+1;
			Cell cell = maze.maze[x][y];
			//if(cell.dtile!=null) continue;
			int side = (int)(((double)(Math.random())*4));
			switch(side) {
			case 0:
				//north
				cell.top = false;
				maze.maze[x][y-1].bottom = false;
				break;
				
				
			case 1:
				cell.bottom = false;
				maze.maze[x][y+1].top = false;
				break;
				
			case 2:
				cell.left = false;
				maze.maze[x-1][y].right = false;
				break;
				
			case 3:
				cell.right = false;
				maze.maze[x+1][y].left = false;
				break;
				
			case 4:
				cell.top = false;
				maze.maze[x][y-1].bottom = false;
				break;
			}
		}
		
		
		Instant end = Instant.now();

		GameHelper.print("Dungeon calculation time took: " + Duration.between(start, end).toMillis() + "ms");

		GameHelper.queue.add(new QueuedAction(){
				public void run(){
					GameHelper.print("Dungeon layout generated, placing blocks!");
				}	
			}
		);

		start = Instant.now();
		
		//Creating dungeon tiles
		for(Cell[] cells : maze.maze) {
			for(Cell cell : cells) {
				DungeonTile t = placeTile(cell.getX(), cell.getY());
				cell.dtile = t;
				
				if(cell.isRoomTile) {
					t.nw = true;
					t.sw = true;
					t.ne = true;
					t.se = true;
				}
			}
		}
		

		for(Rectangle r : rooms) {

			int x1 = r.bottomLeft.x;
			int y1 = r.bottomLeft.y;
			int x2 = x1 + r.getWidth();
			int y2 = y1 + r.getHeight();
			
			//Updating north and south walls
			for(int ___x = x1; ___x < x2; ___x++) {
				

				maze.maze[___x][y1].dtile.nw = false;
				maze.maze[___x][y1].dtile.ne = false;
				

				maze.maze[___x][y2-1].dtile.se = false;
				maze.maze[___x][y2-1].dtile.sw = false;
				//maze.maze[___x][y1].dtile.e = false;
				//maze.maze[___x][y2-1].dtile.w = false;
			}
			
			//Updating east and west walls
			for(int ___y = y1; ___y < y2; ___y++) {
				maze.maze[x1][___y].dtile.nw = false;
				maze.maze[x1][___y].dtile.sw = false;
				

				maze.maze[x2-1][___y].dtile.ne = false;
				maze.maze[x2-1][___y].dtile.se = false;
				//maze.maze[x2-1][___y].right = true;
			}
		}
		
		
		//Updating the tile look
		for(Cell[] cells : maze.maze) {
			for(Cell cell : cells) {
				
				DungeonTile t = cell.dtile;
				t.tileSize = dungeonBrush.tileSize;
				t.e = !cell.isRight();
				t.w = !cell.isLeft();

				t.n = !cell.isTop();
				t.s = !cell.isBottom();
				
				t.updateTilePreview();
			}
		}

		GameHelper.queue.add(new QueuedAction(){
				public void run(){
					GameHelper.print("Finished generating dungeon!");
				}	
			}
		);
		/////////////////////////////////////////////////
		//               POST GENERATION               //
		/////////////////////////////////////////////////
		
		//Top and bottom walls
		/*for(int ___x = 0; ___x < 4; ___x++) {
			int ___tx = 2+___x, ___ty = 2;
			maze.maze[___tx][___ty].top = true;
			maze.maze[___tx][___ty+3].bottom = true;
		}
		//Left and right walls
		for(int ___y = 0; ___y < 4; ___y++) {
			int ___tx = 2, ___ty = 2+___y;
			maze.maze[___tx][___ty].left = true;
			maze.maze[___tx+3][___ty].right = true;
			maze.maze[___tx][___ty].visited = false;
			maze.maze[___tx+3][___ty].visited = false;
		}*/
		
		//Doors
		//Debug.placeSchematic(2+posX+2, posY, 2+posZ+3, "door.north");
		//Debug.placeSchematic(2+posX+2, posY, 2+posZ, "door.south");
		/*end = Instant.now();
		Debug.print("Dungeon block placement took " + Duration.between(start, end).toMillis() + "ms");
		*/
	}

	public String getTileIDString(int tileX, int tileY) {
		return tileX + "," + tileY;
	}
	
	
	public boolean tileExists(int tileX, int tileY) {
		return tiles.containsKey(getTileIDString(tileX, tileY));
	}

	public DungeonTile placeTile(int tileX, int tileZ) {
		DungeonTile t = new DungeonTile(posX+(tileX*2*dungeonBrush.tileSize), posY, posZ+(tileZ*2*dungeonBrush.tileSize));
		t.tileSize = dungeonBrush.tileSize;
		t.tileX = tileX;
		t.tileY = tileZ;
		
		String tileID = getTileIDString(tileX, tileZ);
		if(tiles.contains(tileID)) {
			tiles.remove(tileID);
		}
		//Adds the tile to the hashtable
		tiles.put(tileID, t);
		return t;
		
	}
}
