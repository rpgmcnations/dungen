package me.rater193.plugin;

import static me.rater193.plugin.DunGEN.CHAT_PREFIX;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.io.Files;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.math.transform.AffineTransform;
import com.sk89q.worldedit.util.io.Closer;

import net.md_5.bungee.api.ChatColor;

public class GameHelper {
	public static ArrayList<QueuedAction> queue = new ArrayList<QueuedAction>();
	
	public static void print(Object... objects) {
		String msg = CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED;
		for(int i = 0; i < objects.length; i++) {
			msg += objects[i] + ", ";
		}
		Bukkit.getServer().broadcastMessage(msg);
		
		//(CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED + " Generating");
	}
	
	public static void placeSchematic(int x, int y, int z, String schemName) {

		//Loading the schematic
		try {
			EditSession extent = WorldEdit.getInstance().getEditSessionFactory().getEditSession(new BukkitWorld(Bukkit.getServer().getWorld("World")), -1);
			//WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
			File schematic = new File(DunGEN.getPlugin().getDataFolder() + "/schematics/tiles/"+schemName+".schem");
			//ClipboardFormats.findByAlias(formatName);
	        ClipboardFormat format = ClipboardFormats.findByFile(schematic);
			
			Closer closer = Closer.create();
	        FileInputStream fis = closer.register(new FileInputStream(schematic));
	        BufferedInputStream bis = closer.register(new BufferedInputStream(fis));
	        ClipboardReader reader = closer.register(format.getReader(bis));
	        
	        Clipboard clipboard = reader.read();
	        //ClipboardHolder holder = new ClipboardHolder(clipboard);
	        //Region region = clipboard.getRegion();

	        AffineTransform transform = new AffineTransform();
	        
	        BlockVector3 to = BlockVector3.at
	        		(
	        				x + (clipboard.getOrigin().getX() - clipboard.getMinimumPoint().getX()),
	        				y + (clipboard.getOrigin().getY() - clipboard.getMinimumPoint().getY()),
	        				z + (clipboard.getOrigin().getZ() - clipboard.getMinimumPoint().getZ())
	        				);
	        
	        //Region r = clipboard.getRegion();
	        
			ForwardExtentCopy copy = new ForwardExtentCopy(clipboard, clipboard.getRegion(), clipboard.getOrigin(), extent, to);
	        if (!transform.isIdentity()) copy.setTransform(transform);
	        //if (ignoreAirBlocks) {
	            //copy.setSourceMask(new ExistingBlockMask(clipboard));
	        //}
	        Operations.completeLegacy(copy);
	        extent.flushSession();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static File[] getFilesInDirectory(String directoryName, String extension)
	{
		File dir = new File(directoryName);
		if(dir.isDirectory()) {
			
			File [] files = dir.listFiles(new FilenameFilter() {
				
			    @Override
			    public boolean accept(File dir, String name) {
			        return name.endsWith("."+extension);
			    }
			    
			});
			
			return files;
			
		}
		return null;
	}
	
	public static JSONObject loadJsonFile(File file) {

		String jsonStr = "";
		
		try {
			ArrayList<String> lines = (ArrayList<String>) Files.readLines(file, StandardCharsets.UTF_8);
			for(String line : lines) {
				System.out.println("lines: " + line);
				jsonStr = jsonStr + line;
			}
			
		} catch (IOException e) {
			GameHelper.print("Error: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		

		JSONParser jsonParser = new JSONParser();
		try {
			return (JSONObject)jsonParser.parse(jsonStr);
			
		} catch (ParseException e) {
			GameHelper.print("Error: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		return new JSONObject();
	}
}
