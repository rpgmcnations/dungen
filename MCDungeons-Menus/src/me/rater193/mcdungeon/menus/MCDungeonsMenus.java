package me.rater193.mcdungeon.menus;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class MCDungeonsMenus extends JavaPlugin {

	// Feel free to change this to your own plugin's name and color of your choice.
	public static final String CHAT_PREFIX = ChatColor.AQUA + "MCDungeonMobs";

	private static MCDungeonsMenus plugin; // This is a static plugin instance that is private. Use getPlugin() as seen
									// further below.

	PluginDescriptionFile pdfFile; // plugin.yml

	// Called when the plugin is disabled, such as when you reload the server.
	public void onDisable() {
		
	}

	public static MCDungeonsMenus getPlugin() { // getter for the static plugin instance
		return plugin;
	}
	
	// Called when the plugin is enabled. It is used to set up variables and to register things such as commands.
	@Override
	public void onEnable() {
		plugin = getPlugin(MCDungeonsMenus.class);
		this.getCommand("test").setExecutor(new CmdTest());
		Bukkit.getServer().getPluginManager().registerEvents(new MenuListener(), this);
		ChestMenu.ServerInit(this);
	}
}
