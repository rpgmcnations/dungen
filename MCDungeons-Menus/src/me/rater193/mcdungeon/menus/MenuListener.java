package me.rater193.mcdungeon.menus;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;


public class MenuListener implements Listener {

	@EventHandler
	public void onPickupItem(InventoryClickEvent event) {
		/*
		 * We get the player and make a variable to make it easier to access it when we
		 * need to use it.
		 */
		if(ChestMenu.isMenuItem(event.getCurrentItem())) {
			String playername = event.getWhoClicked().getName();
			
			ChestMenu.handleMenuItem(MCDungeonsMenus.getPlugin().getServer().getPlayerExact(playername), event.getCurrentItem());
			event.setCancelled(true);
		}
		
	}
	
	
}
