package me.rater193.mcdungeon.menus;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class ChestMenu {
	public static NamespacedKey
		key_isMenuItem,
		key_menuItemRunnableID;
	
	public static ArrayList<ChestMenuItemAction> interactions = new ArrayList<ChestMenuItemAction>();
	
	public Inventory inv;
	
	public static void ServerInit(MCDungeonsMenus plugin) {
		key_isMenuItem			= new NamespacedKey(MCDungeonsMenus.getPlugin(), "isMenuItem");
		key_menuItemRunnableID	= new NamespacedKey(MCDungeonsMenus.getPlugin(), "runnableID");
	}
	
	public static boolean isMenuItem(ItemStack stack) {
		if(stack==null) return false;
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer cont = meta.getPersistentDataContainer();
		return cont.has(ChestMenu.key_isMenuItem, PersistentDataType.BYTE);
	}
	
	public ChestMenu(int size, String displayName) {

		inv = Bukkit.createInventory(null, size, displayName);;
		
		/*
		inv.setItem(0, createItem(Material.EMERALD, 100, "Back", new Runnable() {
			@Override
			public void run() {
				System.out.println("Click 1");
			}}));
		inv.setItem(8, createItem(Material.EMERALD, 101, "Next", new Runnable() {
			@Override
			public void run() {
				System.out.println("Click 2");
			}}));
		inv.setItem(3, createItem(Material.EMERALD, 102, "Items", new Runnable() {
			@Override
			public void run() {
				System.out.println("Click 3");
			}}));
		*/
	}
	
	public void setItem(int slot, Material material, int modeldata, String itemName, ChestMenuItemAction r) {
		inv.setItem(slot, createItem(material, modeldata, itemName, r));
	}
	
	public ItemStack createItem(Material mat, int modelid, String name, ChestMenuItemAction r) {
		//Here we are registering our runnable method
		interactions.add(r);
		
		//Here we are creating a new item stack
		ItemStack stack = new ItemStack(mat);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		meta.setCustomModelData(modelid);
		PersistentDataContainer cont = meta.getPersistentDataContainer();
		
		cont.set(key_isMenuItem,			PersistentDataType.BYTE,		(byte)1);
		cont.set(key_menuItemRunnableID,	PersistentDataType.INTEGER,		interactions.indexOf(r));
		
		stack.setItemMeta(meta);
		return stack;
	}
	
	public ItemStack createMenuItem(ItemStack stack, ChestMenuItemAction r) {
		//Here we are registering our runnable method
		interactions.add(r);
		
		//Here we are creating a new item stack
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer cont = meta.getPersistentDataContainer();
		
		cont.set(key_isMenuItem,			PersistentDataType.BYTE,		(byte)1);
		cont.set(key_menuItemRunnableID,	PersistentDataType.INTEGER,		interactions.indexOf(r));
		
		stack.setItemMeta(meta);
		return stack;
	}
	
	public ItemStack createMenuItem(int slot, ItemStack stack, ChestMenuItemAction r) {
		createMenuItem(stack, r);
		inv.setItem(slot, stack);
		return stack;
	}

	public static void handleMenuItem(Player player, ItemStack menuItem) {
		if(isMenuItem(menuItem)) {
			ItemMeta meta = menuItem.getItemMeta();
			PersistentDataContainer cont = meta.getPersistentDataContainer();
			int runnableid = cont.get(ChestMenu.key_menuItemRunnableID, PersistentDataType.INTEGER);
			interactions.get(runnableid).onPlayerClick(player);
		}
	}
}
