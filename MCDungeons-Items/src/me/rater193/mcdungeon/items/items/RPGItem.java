package me.rater193.mcdungeon.items.items;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import me.rater193.mcdungeon.items.MCDungeonItems;

public class RPGItem {
	public static Hashtable<String, RPGItem> items;
	public String itemName;
	public int shopValue;
	public Material materialType;
	public int modelData;
	
	public RPGItem(String itemName, int itemWorth, Material materialType, int modeldata) {
		this.itemName = itemName;
		this.shopValue = itemWorth;
		this.materialType = materialType;
		this.modelData = modeldata;
		//items.put(itemName, this);
	}
	
	public void addStat(ItemStack stack, String statname, int value) {
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		data.set(key, PersistentDataType.INTEGER, value);
		System.out.println("INT ADD?" + value);
		stack.setItemMeta(meta);
	}

	public void addStat(ItemStack stack, String statname, float value) {
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		data.set(key, PersistentDataType.FLOAT, value);
		System.out.println("FLOAT ADD?" + value);
		stack.setItemMeta(meta);
	}

	public void addStat(ItemStack stack, String statname, String value) {
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		data.set(key, PersistentDataType.STRING, value);
		System.out.println("STRING ADD?" + value);
		stack.setItemMeta(meta);
	}
	
	public String getStatStr(ItemStack stack, String statname) {

		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		return data.get(key,  PersistentDataType.STRING);
	}
	
	public int getStatInt(ItemStack stack, String statname) {

		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		return data.get(key,  PersistentDataType.INTEGER);
	}
	
	public float getStatFloat(ItemStack stack, String statname) {

		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer data = meta.getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(MCDungeonItems.getPlugin(), statname);
		return data.get(key,  PersistentDataType.FLOAT);
	}
	
	public ItemStack __newitem() {
		ItemStack stack = new ItemStack(materialType);
		ItemMeta meta = stack.getItemMeta();
		
		meta.setDisplayName(itemName);
		meta.setCustomModelData(modelData);
		
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	public ItemStack createItem() {
		ItemStack stack = __newitem();
		

		ItemMeta meta = stack.getItemMeta();
		List<String> lore = new ArrayList<String>();
		handleItemLore(stack, lore);
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	public void handleItemLore(ItemStack stack, List<String> lore) {
		lore.add(ChatColor.GOLD + "Price: " + ChatColor.WHITE + "" + shopValue + " " + ChatColor.GOLD + "Gold");
	}
}
