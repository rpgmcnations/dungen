package me.rater193.mcdungeon.items.items;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.rater193.mcdungeon.items.EquippableTypes;
import me.rater193.mcdungeon.items.abilities.RPGAbility;

public class RPGItemSkillScroll extends RPGItem {
	
	public EquippableTypes equippableType = EquippableTypes.weapon;
	public RPGAbility ability;

	public RPGItemSkillScroll(String itemName, int itemWorth, Material material, int modelID, String abilityName) {
		super(itemName, itemWorth, material, modelID);
		this.ability = RPGAbility.getAbility(abilityName);
	}

	@Override
	public void handleItemLore(ItemStack stack, List<String> lore) {
		// TODO Auto-generated method stub
		super.handleItemLore(stack, lore);
		lore.add("");
		lore.add(ChatColor.GREEN + "Ability:");
		lore.add(ChatColor.GREEN + "" + ability.displayName);
		lore.add("");
		lore.add(ChatColor.GREEN + "Applies to:");
		lore.add(ChatColor.GREEN + "" + stringifyEquippableType(equippableType));
		lore.add("");
		lore.add(ChatColor.WHITE + "" + ChatColor.BOLD + "Description:");
		lore.add(ChatColor.WHITE + "" + ChatColor.ITALIC + "" + ability.description);
	}
	
	public static String stringifyEquippableType(EquippableTypes type) {
		if(type==EquippableTypes.weapon) {
			return "Weapon";
		}else if(type==EquippableTypes.wand) {
			return "Wand";
		}else if(type==EquippableTypes.armor) {
			return "Armor";
		}
		return "N/A";
	}
}
