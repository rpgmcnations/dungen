package me.rater193.mcdungeon.items.items;

import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class RPGTool extends RPGItem {

	public RPGTool(String itemName, int itemWorth, Material materialType, int modeldata) {
		super(itemName, itemWorth, materialType, modeldata);
	}
	
	@Override
	public ItemStack __newitem() {
		ItemStack stack = super.__newitem();
		
		System.out.println("Getting new item stack");
		addStat(stack, "stats.str", (int)(1 + new Random().nextInt(9)));
		addStat(stack, "stats.att", (int)(1 + new Random().nextInt(9)));
		addStat(stack, "stats.def", (int)(1 + new Random().nextInt(9)));

		return stack;
	}

	@Override
	public void handleItemLore(ItemStack stack, List<String> lore) {
		super.handleItemLore(stack, lore);
		

        lore.add(ChatColor.BLUE + "Str: " + ChatColor.WHITE + "" + getStatInt(stack, "stats.str"));
        lore.add(ChatColor.BLUE + "Att: " + ChatColor.WHITE + "" + getStatInt(stack, "stats.att"));
        lore.add(ChatColor.BLUE + "Def: " + ChatColor.WHITE + "" + getStatInt(stack, "stats.def"));
	}
	
}
