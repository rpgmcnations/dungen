package me.rater193.mcdungeon.items.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rater193.mcdungeon.items.ItemHelper;
import me.rater193.mcdungeon.items.items.RPGItem;
import me.rater193.mcdungeon.items.items.RPGItemSkillScroll;
import me.rater193.mcdungeon.items.items.RPGTool;

public class CmdItem implements CommandExecutor {

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length > 0) {
				switch(args[0]) {
				case "test":
					player.getInventory().addItem(new RPGTool("testtoolname", 0, Material.STICK, 0).createItem());
					break;
				
				case "set":
					player.sendMessage(ChatColor.RED + "Sub command not added, please ask rater to add it, if you need it right now!");
					break;
					
				case "create":
					//String itemName, int itemWorth, Material materialType, int modeldata
					if(args.length>=6) {
						
						switch(args[1]) {
						case "sword":
							//
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.WOODEN_SWORD,Integer.parseInt(args[5])));
							break;
						case "wand":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.STICK,Integer.parseInt(args[5])));
							break;
						case "shield":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.SHIELD,Integer.parseInt(args[5])));
							break;
						case "bow":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.BOW,Integer.parseInt(args[5])));
							break;
						case "helmet":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.IRON_HELMET,Integer.parseInt(args[5])));
							break;
						case "chestplate":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.IRON_CHESTPLATE,Integer.parseInt(args[5])));
							break;
						case "legs":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.IRON_LEGGINGS,Integer.parseInt(args[5])));
							break;
						case"boots":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.IRON_BOOTS,Integer.parseInt(args[5])));
							break;
						case "food":
							ItemHelper.registerItem(args[2], new RPGItem(args[3], Integer.parseInt(args[4]),Material.BREAD,Integer.parseInt(args[5])));
							break;
						case "skill":
							if(args.length>=7) {
								ItemHelper.registerItem(args[2], new RPGItemSkillScroll(args[3], Integer.parseInt(args[4]),Material.PAPER,Integer.parseInt(args[5]), args[6]));
							}else {
								player.sendMessage(ChatColor.RED + "No skill specified!");
								player.sendMessage(ChatColor.RED + "/item create skill <itemID> <itemName> <itemValue> <modelID> <skillID>");
							}
							break;
						default:
							player.sendMessage(ChatColor.RED + "Invalid item type");
							break;
						}
					}else {
						player.sendMessage(ChatColor.RED + "Invalid arguments!");
						player.sendMessage(ChatColor.RED + "/item create <sword|wand|shield|bow|helmet|chestplte|legs|boots|food|skill> <itemID> <itemName> <itemValue> <modelID>");
					}
					break;
					
				case "give":
					if(args.length > 1) {
						player.getInventory().addItem(ItemHelper.getItem(args[1]));
					}else {
						player.sendMessage(ChatColor.RED + "Invalid usage");
						player.sendMessage(ChatColor.RED + "/item give <itemID>");
					}
					break;
					
				case "remove":
					if(args.length > 1) {
						RPGItem.items.remove(ItemHelper.getItem(args[1]));
					}else {
						player.sendMessage(ChatColor.RED + "Invalid usage");
						player.sendMessage(ChatColor.RED + "/item remove <itemID>");
					}
					break;
					
				case "delete":
					if(args.length > 1) {
						RPGItem.items.remove(ItemHelper.getItem(args[1]));
					}else {
						player.sendMessage(ChatColor.RED + "Invalid usage");
						player.sendMessage(ChatColor.RED + "/item remove <itemID>");
					}
					break;
					
				case "list":
					player.sendMessage(ChatColor.YELLOW + "Item IDs: ");
					player.sendMessage(ChatColor.YELLOW + "===================================================");
					int index = 0;
					for(String itemid : ItemHelper.items.keySet()) {
						index+=1;
						player.sendMessage(ChatColor.YELLOW + "" + index + ": " + ChatColor.WHITE + "" + itemid);
					}
					break;
					
				case "load":
					player.sendMessage(ChatColor.RED + "Sub command not added, please ask rater to add it, if you need it right now!");
					break;
				
				default:
					player.sendMessage(ChatColor.RED + "Usage");
					player.sendMessage(ChatColor.RED + "/i <set|create|remove|save|load>");
					break;
					
				}
			}else {
				player.sendMessage("Invalid command usage!");
				return false;
			}
		}else {
			System.out.println("This command can only be executed by a player!");
		}
		return true;
	}

}
