package me.rater193.mcdungeon.items;

public enum EquippableTypes {
	armor,
	weapon,
	wand
}
