package me.rater193.mcdungeon.items;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

import me.rater193.mcdungeon.items.items.RPGItem;

@SuppressWarnings("unchecked")
public class ItemHelper {
	public static Hashtable<String, RPGItem> items = new Hashtable<String, RPGItem>();
	
	public static void registerItem(String itemID, RPGItem item) {
		items.put(itemID, item);
		File plugindir = MCDungeonItems.getPlugin().getDataFolder();
		plugindir.mkdirs();
		
		File itemsfolder = new File(plugindir.getAbsoluteFile() + "/items/");
		itemsfolder.mkdirs();
		
		File itemsfile = new File(itemsfolder.getAbsoluteFile() + "/" + itemID + ".json");
		
		JSONObject jsonobj = new JSONObject();
		jsonobj.put("itemid", itemID);
		jsonobj.put("displayname", item.itemName);
		jsonobj.put("shopvalue", item.shopValue);
		jsonobj.put("modeldata", item.modelData);
		jsonobj.put("materialid", item.materialType.getKey());
		
		String filedata = jsonobj.toJSONString();
		
		try {
			FileWriter wr = new FileWriter(itemsfile);
			wr.write(filedata);
			wr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static ItemStack getItem(String itemID) {
		if(!items.containsKey(itemID)) return null;
		
		return items.get(itemID).createItem();
	}
	
	/*
	public static void handleItem(ItemStack stack) {
		try {
			ItemMeta meta = stack.getItemMeta();
			
			
			meta.setDisplayName(ChatColor.BOLD + "Wifi Stick");
			
			//Setting the item's meta
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GREEN + "Level:");
			lore.add(ChatColor.YELLOW + "1");
			lore.add(ChatColor.BLUE + "XP:");
			lore.add(ChatColor.AQUA + "23/98");
			lore.add(ChatColor.RED + "Damage:");
			lore.add(ChatColor.RED + "Blunt (8 - 12)");
			lore.add("");
			lore.add(ChatColor.LIGHT_PURPLE + "Skill: " + ChatColor.WHITE + "<NONE>");
			lore.add("");
			lore.add(ChatColor.LIGHT_PURPLE + "Socket 1: " + ChatColor.RED + "Fire Aspect III");
			lore.add(ChatColor.LIGHT_PURPLE + "Socket 2: " + ChatColor.WHITE + "<NONE>");
			lore.add(ChatColor.LIGHT_PURPLE + "Socket 3: " + ChatColor.WHITE + "<NONE>");
			lore.add(ChatColor.LIGHT_PURPLE + "Socket 4: " + ChatColor.WHITE + "<NONE>");
			
			meta.setLore( lore );
			stack.setItemMeta(meta);
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("===============================================");
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	public static void givePlayerAbilityScroll(Player p) {
		ItemStack stack = new ItemStack(Material.PAPER);
		ItemMeta meta = stack.getItemMeta();
		meta.setCustomModelData(1);
		
		meta.setDisplayName("Ability Scroll");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GREEN + "Ability:");
		lore.add(ChatColor.GREEN + "Shield Bash");
		lore.add("");
		lore.add(ChatColor.GREEN + "Applies to:");
		lore.add(ChatColor.GREEN + "Shields");
		lore.add("");
		lore.add(ChatColor.WHITE + "" + ChatColor.BOLD + "Description:");
		lore.add(ChatColor.WHITE + "" + ChatColor.ITALIC + "Kock back, and stun enemies with the power of your shield!");
		
		meta.setLore( lore );
		
		stack.setItemMeta(meta);
		p.getInventory().addItem(stack);
	}
	*/
}
