package me.rater193.mcdungeon.items.abilities;

import java.util.Hashtable;

import org.bukkit.entity.Player;

import net.minecraft.server.v1_14_R1.EntityLiving;

public class RPGAbility {
	
	// Static methods and variables are used to let other plugins get access to the plugin
	// Used to store the registered abilities
	public static Hashtable<String, RPGAbility> abilities = new Hashtable<String, RPGAbility>();
	
	// Used to reister abilities
	public static void registerAbility(String abilityName, RPGAbility ability ) {
		if(abilities.containsValue(ability)) return;
		ability.abilityName = abilityName;
		abilities.put(abilityName, ability);
	}
	
	// Used to get stored RPG abilities
	public static RPGAbility getAbility(String abilityName) {
		return abilities.get(abilityName);
	}
	
	public String abilityName;
	public String displayName;
	public String description;
	
	//TODO: Add functionality, onWalk
	public void onWalk(Player player) { }
	//TODO: Add functionality, onDamageTaken
	public void onDamageTaken(Player player, EntityLiving attacker, float damageTaken) { }
	//TODO: Add functionality, onLeftClick
	public void onLeftClick(Player player) { }
	//TODO: Add functionality, onRightClick
	public void onRightClick(Player player) { }
}
