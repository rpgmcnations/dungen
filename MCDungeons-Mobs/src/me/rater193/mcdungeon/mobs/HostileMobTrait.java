package me.rater193.mcdungeon.mobs;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import net.citizensnpcs.api.event.NPCDeathEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.event.NPCSpawnEvent;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;
import net.citizensnpcs.npc.skin.SkinnableEntity;

public class HostileMobTrait extends Trait {
	
	//public List<EntityType> allowedTypes = [];
	List<EntityType> list = Arrays.asList(
			EntityType.BAT,
			EntityType.ZOMBIE,
			EntityType.COW,
			EntityType.PLAYER,
			EntityType.BLAZE,
			EntityType.VILLAGER,
			EntityType.ZOMBIE_VILLAGER,
			EntityType.SKELETON,
			EntityType.DROWNED
			);
	
	private int targetselectorRate = 10;
	private int targetselectorPlace = 0;

	public HostileMobTrait() {
		super("hostilemob");
	}
	
	public String
			skinData =
				"eyJ0aW1lc3RhbXAiOjE1NjY2NTA3NTQ4NTUsInByb2ZpbGVJZCI6IjRlY2JlNjM0ZmYzYzQ2YmZhZDAxODdjMGJiMjVlYzQzIiwicHJvZmlsZU5hbWUiOiJHb2JsaW4iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzhlYjI4MWY2YTE4NWVmN2VkNzc4Yzk0MjZiMmVlZjM5OWNlZDVhMGVlN2M1YjczNjVkNzM0YzIxODI1NTZiMWEifX19",
			skinSignature =
				"GFdpscptWOC29Vb4ynx0hTOu3XqE54afyxo1v9qJERk6hh3WRyLqzt+38UkZOeTScUFG9Kg1doO2TcfJo2/wQ6Dx8Flh4B0ePu+vOazWuEKyeoP/tvESPrkmbKYFSgPqSJK61Yv1f/X0CTc3aj2cG5jCWKtuLJeWufr3asDCo89OvlWjlyQVRUTXAXxUYP8+0JIgspiKDdJIhHgdLKyu/P6v+JHYzixJPfkl9I38m8hstPY+bdMJrunvkdFp6jmflFE1fcmXprh2GMP5xdHYeuzbxxMRfwora1XLyM45WieRumVoBKrTVraPxM/fCgGr8mSr1cdXqhd4Fb9UiLv+AZ4bHhm4tJL2S5njVwL8bMeH//X6g7+lS+pCTOxjZxt/myYt+LBSRUn0Bip01M/0fL8ad315YGzpknqO1IP5tcV+uolgRDpT5mNH1Ibaeqg+4pMqUaODD9vnlOayA65FFSNCLXwc9XRfKQ88MsW6T8WpAFQfDlCw87n/k12re02g9yJSaEeTPMqg8mtI1HcgQRAY2IT9CRfjlY8167RLcTszB5oeoNYJUHAuPMOVHfai4H4+zGH17//pL5MlEkw/Jbfv4nd3P0uhIFujT66dq3T0umXmotGYddeOyOIZBLODwR61TwfU18u8NowCKuvT2sba+EWa2Dhu5BnWXGINcUc=";
	
	
	public void updateSkin() {
		if(npc.getEntity() instanceof SkinnableEntity) {
			((SkinnableEntity)npc.getEntity()).setSkinPersistent(npc.getName().toLowerCase(),
					this.skinSignature,
					this.skinData);
		}
	}

	
	public void load(DataKey key) {
		//SomeSetting = key.getBoolean("SomeSetting", false);
	}

	// Save settings for this NPC (optional). These values will be persisted to the Citizens saves file
	public void save(DataKey key) {
		//key.setBoolean("SomeSetting",SomeSetting);
		//key.setInt("Size", menu.inv.getSize());
	}
	
	@EventHandler
	public void click(NPCRightClickEvent event){
		//update();
		//event.getClicker().openInventory(menu.inv);
	}
	
	@EventHandler
	public void onDeath(NPCDeathEvent event) {
		System.out.println("DIE");
		//event.getNPC().despawn(DespawnReason.DEATH);
		event.setDroppedExp(1000);
		event.getNPC().destroy();
	}
	
	@EventHandler
	public void onSpawn(NPCSpawnEvent event) {
		System.out.println("SPAWN");
		//event.getNPC().despawn(DespawnReason.DEATH);
		//event.getNPC().getEntity().setCustomNameVisible(true);
		//event.getNPC().getEntity().setCustomName("TestName");
		
		//event.getNPC().getEntity().getPersistentDataContainer().st
	}
	
	@Override
	public boolean isRunImplemented() {
		// TODO Auto-generated method stub
		return true;
	}



	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		
		// TODO Auto-generated method stub
		//System.out.println("a");
		targetselectorPlace += 1;
		if(targetselectorPlace >= targetselectorRate) {
			targetselectorPlace = 0;

			//This updates the skin
			updateSkin();
			
			
			Entity e = this.npc.getEntity();
			
			if(this.npc.getNavigator().getEntityTarget()==null) {
				if(e instanceof LivingEntity) {
					LivingEntity mob = (LivingEntity) e;
					mob.setCustomNameVisible(true);
					
					
					List<Entity> entities = this.npc.getEntity().getNearbyEntities(24, 24, 24);
					for(Entity ent : entities) {
						if(ent instanceof Player) {
							Player p = (Player) ent;
							if(!p.hasMetadata("NPC")) {
								//boolean isCitizensNPC = entity.hasMetadata("NPC");
								this.npc.getNavigator().setTarget(p, true);
							}
						}
					}
					
					//this.npc.getNavigator().setTarget(Bukkit.getServer().getPlayer("rater193"), true);
					this.npc.getNavigator().setPaused(false);
					/*
					GoalController controller = this.npc.getDefaultGoalController();
					controller.setPaused(false);
					if(!controller.isExecutingGoal()) {
						//controller.addGoal(arg0, arg1);
						controller.addPrioritisableGoal(new PrioritisableGoal() {
		
							@Override
							public void reset() {
								
							}
		
							@Override
							public void run(GoalSelector selector) {
								
							}
		
							@Override
							public boolean shouldExecute(GoalSelector arg0) {
								return false;
							}
		
							@Override
							public int getPriority() {
								return 0;
							}
							
						});
					}
					*/
					
					
					//String title = "";
					//title += ChatColor.GRAY + "[lv.5] Basic Zombie";
					
					//mob.setCustomNameVisible(true);
					//mob.setCustomName(title);
				}
			}
		}
		
		
		//e.setCustomNameVisible(true);
		//e.setCustomName(e.getName() + " " + ChatColor.RED + "( " + e.);
		/*
		if(e instanceof ArmorStand) {
			//System.out.println("Armor stand");
			ArmorStand as = (ArmorStand)e;
			
			as.setArms(true);
			as.setBasePlate(false);
			//as.setRotation(as.set, 0);
			//as.getLeftArmPose().add(0, 0, 1);
			//as.getEyeLocation().setYaw(45);

			double ax = as.getLeftArmPose().getX();
			double ay = as.getLeftArmPose().getY();
			double az = as.getLeftArmPose().getZ();
			
			as.setLeftArmPose(new EulerAngle(ax, ay, -Math.sin(test*0.5)-(Math.PI/2.5f)));
			
			as.getLocation().setYaw(as.getLocation().getYaw() + 5);

			//ax = as.getBodyPose().getX();
			//ay = as.getBodyPose().getY();
			//az = as.getBodyPose().getZ();
			
			//as.setBodyPose(new EulerAngle(ax, ay+5, az));
			
		}else {
			//System.out.println("No armor stand");
		}
		*/
		
	}



	@Override
	public void onAttach() {
		System.out.println("On attach");
		this.npc.data().set("damageMin", 2);
		this.npc.data().set("damageMax", 5);
		this.npc.data().set("speed", 10f);
		this.npc.data().set("defence", 0);
		this.npc.setName(ChatColor.GRAY + "[lv.5] " + ChatColor.WHITE + "Basic Zombie");
		this.npc.setProtected(false);
		this.npc.getNavigator().getLocalParameters().stuckAction(null);
		this.npc.getNavigator().getDefaultParameters().speedModifier(2);

		//this.npc.data().set(NPC.PLAYER_SKIN_USE_LATEST, false);
		//this.npc.data().set(NPC.PLAYER_SKIN_UUID_METADATA, "a39ef57a-387c-4cd1-9740-59b732ebc7a5");
		//this.npc.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_METADATA, "eyJ0aW1lc3RhbXAiOjE1NjM3OTQ1OTYwOTYsInByb2ZpbGVJZCI6ImEzOWVmNTdhMzg3YzRjZDE5NzQwNTliNzMyZWJjN2E1IiwicHJvZmlsZU5hbWUiOiJNZXJjaGFudCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY0ZDQzMTY1MWQ4Y2U3ZGEyZDhmODhkZDgyMmQ5NTU3ZDdiY2Y5YjBiNWQwYThlZDEzYjk4OTg4MWZhZGJlZiJ9fX0=");
		//this.npc.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_SIGN_METADATA, "FB856zZB5l5bwd0+GDJKqDSgK/FA5Re47HDrwfPeM0+T0++CMIHcfpzRQH9ITZxbhUn7lxASw7DKvA8ndFJBt1PcjRehNrYxsFUzzBXfgTo9/V4hDcjc1E4gFrNn1pvAyAI2HNaEbyRuBpIxZcKmkzREc603Y/Y0zCNYSjYZ6/mrz0cc6EDZDWkEl0e0HBxTE336F+OCrIfcSOLgVbG/6ilt63ONLLnNjDOeEBNOtifEPXeCsWzaGxDCMkhgCPD6AsoHddsrEeBr5zU6Ql3xW26+MANx8IVpTAF+0xWMvMIOe99XRRGmCpxk74s/sN6PC1vIHcXjyohU3DS7WYdtYGbEty/fmE1UoUngRW/09KYqAb52QtuQtcFtBizqr0djd6k7z1Hvg38FaWn9Xmq088EJZfEDIHW1KZNqCA9RtMevtC7MuYw5fbzZEqCXq+P+/xdT12RpW301yArsB2/bRaD6UIcK4H0/m9qYwaVc4tYbbfKoqrnaLHn8X+kS2sozlrnhNzSJL5zkkLOhOTVOiUnwTH9X8wa2JXHuarsnBI5G4jhHNdjF/e5o8eoOZJMgMWBLEO/JozVac8JYMWTX2UxoSOaIwBF3+fdLsl80JyciiCogNR3LGUX9SSXP/vLRclZGgijlQMCgMAf1FTJMEtUJlTBMx7v6scP4kq5vxh0=");
		
		//this.npc.getNavigator().getDefaultParameters().attackStrategy(AttackStrategy);
		//this.npc.getNavigator().getDefaultParameters().attackRange(2);
		//this.npc.getNavigator().getDefaultParameters().;


		Entity e = this.npc.getEntity();
		
		if(e instanceof LivingEntity) {
			LivingEntity mob = (LivingEntity) e;
			
			mob.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
			mob.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
			mob.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
			mob.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
			mob.getEquipment().setItemInMainHand(new ItemStack(Material.BOW));
		}
		
		updateSkin();
		
		
		if(npc.getEntity() instanceof LivingEntity) {
			LivingEntity ent = (LivingEntity)npc.getEntity();
			
			ent.setMaxHealth(100);
			ent.setHealth(ent.getMaxHealth());

		}
		super.onAttach();
	}

}
