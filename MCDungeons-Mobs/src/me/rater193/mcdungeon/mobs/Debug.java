package me.rater193.mcdungeon.mobs;

import static me.rater193.mcdungeon.mobs.MCDungeonMobs.CHAT_PREFIX;

import org.bukkit.Bukkit;


import net.md_5.bungee.api.ChatColor;

public class Debug {
	public static void print(Object... objects) {
		String msg = CHAT_PREFIX +  ChatColor.WHITE + " > " + ChatColor.RED;
		for(int i = 0; i < objects.length; i++) {
			msg += objects[i] + ", ";
		}
		Bukkit.getServer().broadcastMessage(msg);
	}
}
